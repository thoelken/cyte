#!/usr/bin/env python3
from argparse import ArgumentParser as AP
from prompt_toolkit import prompt
from prompt_toolkit.history import InMemoryHistory
import urllib.request
import urllib.parse
import json
import tinydb
import sys
# import sqlite3
import logging as log

cli = AP(description='command line citation manager')
cli.add_argument('-v', '--verbose', action='store_true', help='print verbose information to STDERR')
cli.add_argument('-d', '--database', default='library.db', metavar='FILE',
                 help='database file (default:"./library.db")')
cmds = cli.add_subparsers(dest='cmd')
cmd_query = cmds.add_parser('query', aliases=['q'])
cmd_query.add_argument('type', choices=['doi', 'author', 'title', 'all'])
args = cli.parse_args()
log.basicConfig(level=log.DEBUG if args.verbose else log.WARN, format='[%(levelname)s] %(message)s')

db = tinydb.TinyDB(args.database)

qry = AP()
qry.add_argument('-a', '--author', nargs='+')
qry.add_argument('-t', '--title', nargs='+')
qry.add_argument('-j', '--journal', nargs='+')

history = InMemoryHistory()


def query_mode(query=''):
    if not query:
        query = prompt('Type "h" for help. Search query:> ', history=history)
        if query == 'h':
            print('-a author, -t title, -j journal for specific queries, otherwise full text')
            return query_mode()
    query = query.split()
    q = {}
    if [1 for word in query if word.startswith('-')]:
        parsed = qry.parse_args(query)
        if parsed.author:
            q['query.author'] = parsed.author
        if parsed.title:
            q['query.title'] = parsed.title
        if parsed.journal:
            q['query.container-title'] = parsed.journal
    else:
        q['query'] = query
    url = 'https://api.crossref.org/works?%s' % urllib.parse.urlencode(q)
    request = urllib.request.Request(url)
    print(request.full_url)

    # request.add_header('accept', 'application/citeproc+json')
    result = json.loads(urllib.request.urlopen(request).read().decode('utf-8'))
    for i, r in enumerate(result['message']['items']):
        author = r['publisher']
        if 'author' in r:
            author = r['author'][0]['family']
            # if len(r['author']) > 1:
            #     author += ' et al.'
        year = r['created']['date-parts'][0][0]
        title = r['title'][0]
        journal = r['short-container-title'][0] if 'short-container-title' in r else ''
        doi = r['DOI']
        sys.stdout.write('[%d] %s %d; %s. %s %s\n' % (i, author, year, title, journal, doi))
    action = prompt('Action: add #, back, query > ', history=history)
    if action.startswith('b'):
        return query_mode()
    if action.startswith('q'):
        return query_mode(action.split(' ', 1)[1] if ' ' in action else '')
    if action.startswith('a'):
        num = 0
        if len(result['message']['items']) > 1 and ' ' in action:
            num = int(action.split(' ', 1)[1])-1
        add_to_library(result['message']['items'][num]['DOI'])
    return query_mode()


def add_to_library(doi):
    request = urllib.request.Request('https://dx.doi.org/' + doi)
    # request.add_header('accept', 'text/bibliography; style=bibtex')
    request.add_header('accept', 'application/citeproc+json')
    result = json.loads(urllib.request.urlopen(request).read().decode('utf-8'))
    print(result)


def library_mode():
    pass


loop = True
while loop:
    cmd = prompt('Select an action:\n0 Exit\nq Query\nl Library\n> ', history=history)
    if cmd == '0':
        break
    if cmd.startswith('q'):
        query = cmd.split(' ', 1)[1] if ' ' in cmd else ''
        query_mode(query)
    if cmd == 'l':
        library_mode()
